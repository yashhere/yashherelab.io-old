---
comments: false
slug: whoami
title: "whoami"
type: page
---

> None of my writings started out as a book project.
I wrote my first book – Seeking Wisdom – as a memorandum for myself with the expectation that I could transfer some of its essentials to my children.
I learn and write because I want to be a little wiser day by day.
I don’t want to be a great-problem-solver. I want to avoid problems – prevent them from happening and doing right from the beginning.
And I focus on consequential decisions.
To paraphrase Buffett and Munger – decision-making is not about making brilliant decisions, but avoiding terrible ones. Mistakes and dumb decisions are a fact of life and I’m going to make more, but as long as I can avoid the big or “fatal” ones I’m fine.

> — Peter Bevelin

<div style="width:50%; margin: 30px auto"><img style=" width: 200px; height: 200px; border-radius: 2%; margin-left: auto; margin-right: auto; display: block;" src="/images/yash.jpg"></div>
Hi there,<br>
I am Yash Agarwal(यश अग्रवाल, [y-uh-sh](https://www.youtube.com/watch?v=mzJVVcVVbA4)). A final year undergrad at Department of Computer Science and Engineering at NIT Calicut, India.

I used to be a super studious guy till my 10<sup>th</sup> standard, so I followed the _herd_ and started preparing for IIT JEE. Those two years of coaching taught me a lot of life lessons, which came handy later in my life. Anyhow, two years of slogging and hard work landed me in NIT Calicut, a decision I'll never regret. For someone who never wrote a single line of code before coming to college, to a person who can code in any language you throw, NIT Calicut has given me a lot.

Now, I am a changed guy. There's much more to me rather than just studying. I tend to invest my time watching movies, reading blogs, hanging out on Twitter and enjoying my life in general.

I love trying new operating systems, new tech, and new programming languages. Well, pretty much everything related to the tech. I am a big fan of Linux and can talk anything about it. My first interaction with Linux was in the second year of under-graduation during Scheme programming lab (yeah, you read it right!). I immediately fall in love with it. Unlike most of my classmates, I started using Linux for my daily use, not just for assignments. I learned a lot in the process. I did have to spend a lot of time just hacking around in Linux, to reach this level. That reflects in my CGPA too :innocent:.

As for writing, writing helps me vent out my ideas and other thoughts that otherwise would be lost. I believe that when you read something, you simply convince yourself that you understand it properly. But when you write down the same thing, you have to prove that you understand it properly by putting it into meaningful sentences. In that way, writing has helped me put together my knowledge in a systematic manner. One other motivation was to document some of my life experiences so that somebody else can take benefit from it.

Now that you know a lot about me, go ahead, and read some of the stuff on this blog.

And add me to your [RSS](/index.xml) reader.


#### Stats for nerds:

1. My current operating system is:
    + ~~Arch Linux and Gnome Desktop~~(September '17 to November '17)
    + ~~Linux Mint and i3~~(November '17 to January '18)
    + ~~Arch Linux and i3~~(January '18 to present)
    + Ubuntu and i3(March '18 to present)
<br/>
2. This blog is built using [Hugo](https://gohugo.io/), a static site generator. The theme used is [minimo](https://themes.gohugo.io/minimo). The source code of this blog can be found on [GitLab](https://gitlab.com/yashhere/yashhere.gitlab.io). A mirror exists at [GitHub](https://github.com/yashhere/yashhere.github.io) also. This site is automatically built using GitLab CI for GitLab and [Wercker](http://www.wercker.com) for GitHub. So this site is accessible at both [`yashhere.gitlab.io`](https://yashhere.gitlab.io) and [`yashhere.github.io`](https://yashhere.github.io).


Ping me on Matrix where I hang out as `@im_y:matrix.org` and `@yashhere:matrix.org`.

You can also find me at:

1. **Email:** yashagarwaljpr [at] gmail [dot] com
2. [Twitter](https://www.twitter.com/yash__here)
3. [GitHub](https://www.github.com/yashhere)
4. [GitLab](https://www.gitLab.com/yashhere)
