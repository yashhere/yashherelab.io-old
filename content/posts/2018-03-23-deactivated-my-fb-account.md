---
date: "2018-03-23T21:00:44+05:30"
title: "Deactivated My Facebook Account"
categories:
  - life
  - experience
  - Thoughts
  - updates
tags:
  - NITC
---

Today, I deactivated my Facebook account again. For last few days, I started feeling that I am using Facebook excessively. That was affecting some other works. It was even affecting my *public life*. I was using Facebook day in and day out. Today when I woke up in the morning, I first opened Facebook and didn't leave my bed for one hour. I realized that I have wasted one hour merely browsing through useless stuff. Immediately, I decided to deactivate the account for some time and analyze how much my life changes by one less distraction.

Coincidentally, it is the time of [Ragam](http://ragam.org.in/Main/), the annual cultural festival of NIT Calicut. I don't know why, but I *introspect* myself every year at the same time. For last two years, this introspection is resulting in deactivation of my Facebook account. It is the reason, why I am able to finish all my pending projects in the summers.

I sometimes feel that after coming to NITC, I have become much more socially awkward and even more of a loner than I was before. I never enjoyed going into public events, but in college, that tendency seems to have increased a lot. Now, I do not feel comfortable with this state. I want to change this, but every time I try, there is some *invisible force* which pulls me back from expressing myself. NITC's environment was entirely different for a person like me who always preferred to stay alone. So, I built a *bubble* around myself, where there is no one to disturb or doubt me. This was good in initial years of my campus life when circumstances were not in my favor, but now this bubble is an obstacle for me. I feel difficulty in coming out of this bubble.

There are only one and half months left of my undergraduate life. I might not overcome this fear in this time. But I hope that the change of environment will help me cross this barrier. I have learnt a lot of lessons from my mistakes in the past, and I hope that I will not repeat one of the biggest mistakes of my life again.

I will write again about the changes I feel after deactivating the Facebook. In the meantime, I will try to spend more time with my friends and less with my laptop. :relieved:
